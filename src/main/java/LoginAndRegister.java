import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.Arrays;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class LoginAndRegister {

    private static final By LOC_MY_ACCOUNT = By.linkText("My Account");
    private static final By LOC_REGISTER = By.linkText("Register");
    private static final By LOC_LOGIN = By.linkText("Login");
    private static final By LOC_LOGOUT = By.linkText("Logout");
    private static final By LOC_CONTINUE = By.linkText("Continue");
    private static final By LOC_LOGIN_EMAIL = By.name("email");
    private static final By LOC_LOGIN_PASSWORD = By.name("password");
    private static final By LOC_LOGIN_BUTTON = By.cssSelector("input[value='Login']");
    private static final By LOC_FIRSTNAME = By.id("input-firstname");
    private static final By LOC_LASTNAME= By.id("input-lastname");
    private static final By LOC_EMAIL= By.id("input-email");
    private static final By LOC_TELEPHONE= By.id("input-telephone");
    private static final By LOC_PASSWORD= By.id("input-password");
    private static final By LOC_CONFIRM= By.id("input-confirm");
    private static final By LOC_SUBSCRIBE= By.xpath("//*[@name='newsletter'][@value='1']");
    private static final By LOC_PRIVACY= By.name("agree");
    private static final By LOC_CONTINUE_BUTTON= By.cssSelector("input[value='Continue']");
    private static final By LOC_FIRSTNAME_ERROR=By.xpath("//div[contains(text(),'First Name')]");
    private static final By LOC_LASTNAME_ERROR=By.xpath("//div[contains(text(),'Last Name')]");
    private static final By LOC_EMAIL_ERROR=By.xpath("//div[contains(text(),'E-Mail')]");
    private static final By LOC_TELEPHONE_ERROR= By.xpath("//div[contains(text(),'Telephone')]");
    private static final By LOC_PASSWORD_ERROR= By.xpath("//div[contains(text(),'Password')]");
    private static final By LOC_CREATED_ACCOUNT_MESSAGE = By.cssSelector("div[id='content']");


    /**
     * Method which takes you to the page "shop.pragmatic.bg"
     */
    public static void goTo() {
        Browser.driver.get("http://shop.pragmatic.bg");
    }


    public static void register(String firstname, String lastname, String email, String telephone, String password, String confirm ) {

        Browser.driver.findElement(LOC_MY_ACCOUNT).click();
        Browser.driver.findElement(LOC_REGISTER).click();
        Browser.driver.findElement(LOC_FIRSTNAME).sendKeys(firstname);
        Browser.driver.findElement(LOC_LASTNAME).sendKeys(lastname);
        Browser.driver.findElement(LOC_EMAIL).sendKeys(email);
        Browser.driver.findElement(LOC_TELEPHONE).sendKeys(telephone);
        Browser.driver.findElement(LOC_PASSWORD).sendKeys(password);
        Browser.driver.findElement(LOC_CONFIRM).sendKeys(confirm);
        Browser.driver.findElement(LOC_SUBSCRIBE).click();
        Browser.driver.findElement(LOC_PRIVACY).click();
        Browser.driver.findElement(LOC_CONTINUE_BUTTON).click();



    }
    /**
     * Method which logins the user
     */
    public static void login(String mail, String password){
        Browser.driver.findElement(LOC_MY_ACCOUNT).click();
        Browser.driver.findElement(LOC_LOGIN).click();
        Browser.driver.findElement(LOC_LOGIN_EMAIL).sendKeys(mail);
        Browser.driver.findElement(LOC_LOGIN_PASSWORD).sendKeys(password);
        Browser.driver.findElement(LOC_LOGIN_BUTTON).click();

    }
    /**
     * Method which verifies that the registration is unsuccessful
     *
     */
    public static void verifyRegisterErrorMessages(){
        List<String> exp_options = Arrays.asList("First Name must be between 1 and 32 characters!","Last Name must be between 1 and 32 characters!","E-Mail Address does not appear to be valid!","Telephone must be between 3 and 32 characters!","Password must be between 4 and 20 characters!");
        List<String> act_options = Arrays.asList(Browser.driver.findElement(LOC_FIRSTNAME_ERROR).getText(),Browser.driver.findElement(LOC_LASTNAME_ERROR).getText(),Browser.driver.findElement(LOC_EMAIL_ERROR).getText(),Browser.driver.findElement(LOC_TELEPHONE_ERROR).getText(),Browser.driver.findElement(LOC_PASSWORD_ERROR).getText());
        assertEquals(act_options.toString(), exp_options.toString());
        Assert.assertTrue(act_options.toString().contains(exp_options.toString()));
    }

    /**
     * Method which verifies that the registration is successful
     *
     */
    public static void verifyRegistration(){
        String expectedMessage = "Your Account Has Been Created!\n" +
                "Congratulations! Your new account has been successfully created!\n" +
                "You can now take advantage of member privileges to enhance your online shopping experience with us.\n" +
                "If you have ANY questions about the operation of this online shop, please e-mail the store owner.\n" +
                "A confirmation has been sent to the provided e-mail address. If you have not received it within the hour, please contact us.\n" +
                "Continue";
        String actualMessage = Browser.driver.findElement(LOC_CREATED_ACCOUNT_MESSAGE).getText();
        assertEquals(expectedMessage,actualMessage);
    }

    /**
     * Method which logouts the user
     */
    public static void logout(){
        Browser.driver.findElement(LOC_MY_ACCOUNT).click();
        Browser.driver.findElement(LOC_LOGOUT).click();
        Browser.driver.findElement(LOC_CONTINUE);
    }


}
