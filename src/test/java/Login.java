import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Login {
    @BeforeMethod
    public void setUp() {
        Browser.open("chrome");
    }

    @Test
    public void loginTest(){
        LoginAndRegister.goTo();
        LoginAndRegister.login("dimiter.traykoV@abv.bg","123456");

        LoginAndRegister.logout();
    }

    @AfterMethod
    public void tearDown() {
        Browser.quit();
    }
}
