import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CorrectRegister {
    private static final By LOC_CONTINUE = By.linkText("Continue");
    @BeforeMethod
    public void setUp() {
        Browser.open("chrome");
    }

    @Test
    public void RegisterTest(){
        LoginAndRegister.goTo();
        LoginAndRegister.register("Dimitar","Traykov","dimiter.traykoV@abv.bg","0879000706","123456","123456");
        LoginAndRegister.verifyRegistration();
        Browser.driver.findElement(LOC_CONTINUE).click();
    }
    @AfterMethod
   public void tearDown() {
        Browser.quit();
    }
   }

