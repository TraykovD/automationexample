import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class IncorrectRegister {
    private static final By LOC_CONTINUE = By.linkText("Continue");
    @BeforeMethod
    public void setUp() {
        Browser.open("chrome");
    }

    @Test
    public void EmptyFieldsTest(){
        LoginAndRegister.goTo();
        LoginAndRegister.register("","","","","","");
        LoginAndRegister.verifyRegisterErrorMessages();
    }
    @AfterMethod
    public void tearDown() {
        Browser.quit();
    }
}
